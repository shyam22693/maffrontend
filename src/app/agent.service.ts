import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { catchError, tap, map } from 'rxjs/operators';
// import 'rxjs/add/operator/map'


let httpHeaders = new HttpHeaders({
     'content-Type' : 'application/json',
   
}); 

const httpOptions = {
  headers: httpHeaders
};


@Injectable({
  providedIn: 'root'
})
export class AgentService {

//shyam added
 constructor(private http: HttpClient) { }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message);
    } else {
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    return throwError('Something bad happened; please try again later.');
  };

  private extractData(res: Response) {
    let body = res;
    return body || { };
  }
  //First Function to Save Data Into Register
  postAgent(data): Observable<any> {
    return this.http.post('https://q4ph7hsmg6.execute-api.us-east-1.amazonaws.com/realstage/agent/registration', data, httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }

//shyam Added
}
