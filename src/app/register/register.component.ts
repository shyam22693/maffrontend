import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';
import { FormControl, FormGroupDirective, FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';

import { AgentService } from '../agent.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
registerForm:FormGroup



  // SH**1
  constructor(private router: Router, private fb: FormBuilder, private agentApi : AgentService) { }
  ngOnInit() {
	this.registerForm = this.fb.group({
	    agentId: [null, Validators.required],
	    sponserId: [null, Validators.required],
	    firstName: [null, Validators.required],
	    lastName: [null, Validators.required],
	    gender: [null, Validators.required],
	    birthDate: [null, Validators.required],
	    phoneNo: [null, Validators.required],
	    emailId: [null, Validators.required],
	    ssnNumber: [null, Validators.required],
	    residentAddress: [null, Validators.required],
	    residentSuitApartment: [null, Validators.required],
	    residentCity: [null, Validators.required],
	    residentState: [null, Validators.required],
	    residentZipcode: [null, Validators.required],
	    residentCountry: [null, Validators.required]
	  });
  }
  onFormSubmit(registerForm:NgForm) {
    console.log('registerForm',registerForm);
    this.agentApi.postAgent(registerForm)
      .subscribe(res => {
          this.router.navigate(['/']);
        }, (err) => {
          console.log(err);
        });
  }

}
